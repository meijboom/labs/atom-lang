use anyhow::Result;
use regex::bytes::Regex;

use crate::syntax::{ArithmeticExpr, ArithmeticOp, CallExpr, Cmd, ComparisonExpr, ComparisonOp, Expr, Field, PropertyExpr, Select, Selector};

pub struct Codegen {
    strict: bool,
}

impl Codegen {
    pub fn new(strict: bool) -> Self {
        Self {
            strict
        }
    }

    fn escape(&self, value: &str) -> Result<String> {
        if self.strict {
            return Ok(format!("\"{}\"", value));
        }

        let safe_ident = Regex::new(r"^[a-zA-Z][a-zA-Z0-9_]+$")?;

        if safe_ident.is_match(value.as_ref()) {
            return Ok(value.to_string());
        }

        Ok(format!("\"{}\"", value))
    }

    fn alias(&self, value_left: &str, value_right: &str) -> Result<String> {
        if !self.strict && value_left == value_right {
            return Ok(value_left.to_string());
        }

        Ok(format!("{} as {}", value_left, value_right))
    }

    fn call(&self, callee: Option<&Expr>, call: &CallExpr) -> Result<String> {
        let mut args = vec![];

        if let Some(expr) = callee {
            args.push(self.expr(expr)?);
        }

        for arg in call.args.iter() {
            args.push(self.expr(arg)?);
        }

        Ok(format!("{}({})", call.name.to_uppercase(), args.join(", ")))
    }

    fn property(&self, property: &PropertyExpr) -> Result<String> {
        match &property.property {
            Expr::Call(call) => self.call(Some(&property.object), call),
            _ => Ok(format!("{}.{}", self.expr(&property.object)?, self.expr(&property.property)?))
        }
    }

    fn not(&self, expr: &Expr) -> Result<String> {
        Ok(format!("NOT {}", self.expr(expr)?))
    }

    fn arithmetic(&self, arithmetic: &ArithmeticExpr) -> Result<String> {
        let op = match arithmetic.op {
            ArithmeticOp::Add => "+",
            ArithmeticOp::Sub => "-",
            ArithmeticOp::Mul => "*",
            ArithmeticOp::Div => "/",
            ArithmeticOp::BitAnd => "&",
            ArithmeticOp::BitOr => "|",
        };

        Ok(format!("{} {} {}", self.expr(&arithmetic.left)?, op, self.expr(&arithmetic.right)?))
    }

    fn comparison(&self, comparison: &ComparisonExpr) -> Result<String> {
        let op = match comparison.op {
            ComparisonOp::Lt => "<",
            ComparisonOp::Lte => "<=",
            ComparisonOp::Gt => ">",
            ComparisonOp::Gte => ">=",
            ComparisonOp::Eq => "=",
            ComparisonOp::Neq => "!=",
        };

        Ok(format!("{} {} {}", self.expr(&comparison.left)?, op, self.expr(&comparison.right)?))
    }

    fn int(&self, value: i64) -> Result<String> {
        Ok(format!("{}", value))
    }

    fn expr(&self, expr: &Expr) -> Result<String> {
        match expr {
            Expr::Int(num) => self.int(*num),
            Expr::Ident(name) => self.escape(&name),
            Expr::Call(call) => self.call(None, call),
            Expr::Property(prop) => self.property(prop),
            Expr::Not(expr) => self.not(expr),
            Expr::Arithmetic(arithmetic) => self.arithmetic(arithmetic),
            Expr::Comparison(comparison) => self.comparison(comparison),
        }
    }

    fn field(&self, field: &Field) -> Result<String> {
        self.alias(&self.expr(&field.expr)?, &self.escape(&field.name)?)
    }

    fn selector(&self, selector: &Selector) -> Result<String> {
        let fields = match &selector.name {
            Some(name) => self.alias(
                &self.escape(&selector.table_name)?,
                &self.escape(name)?,
            )?,
            None if !self.strict => self.alias(
                &self.escape(&selector.table_name)?,
                &self.escape(&selector.table_name)?,
            )?,
            None => self.escape(&selector.table_name)?,
        };

        if let Some(expr) = &selector.filters {
            return Ok(format!("{} WHERE {}", fields, self.expr(expr)?));
        }

        Ok(fields)
    }

    fn select(&self, select: &Select) -> Result<String> {
        let mut fields = vec![];

        for field in select.fields.iter() {
            fields.push(self.field(&field)?);
        }

        Ok(format!("SELECT {} FROM {}", fields.join(", "), self.selector(&select.selector)?))
    }

    pub fn generate(&self, cmd: &Cmd) -> Result<String> {
        match cmd {
            Cmd::Select(select) => self.select(&select),
        }
    }
}