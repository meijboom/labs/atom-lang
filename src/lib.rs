use std::ffi::{CStr, CString};
use std::os::raw::c_char;

use anyhow::Result;

use codegen::Codegen;
use syntax::parser;

mod syntax;
mod codegen;

#[no_mangle]
pub extern "C" fn eval_aql(input: *const c_char) -> *const c_char {
    unsafe {
        let output = match eval(CStr::from_ptr(input).to_str().unwrap()) {
            Ok(output) => output,
            Err(e) => format!("RAISE EXCEPTION '{}'", e).trim().to_string(),
        };

        CString::new(output).unwrap().into_boxed_c_str().as_ptr()
    }
}

fn eval(input: &str) -> Result<String> {
    let cmd = parser::parse(input)?;
    let output = Codegen::new(false).generate(&cmd)?;

    Ok(output)
}
