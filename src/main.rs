use anyhow::Result;

use codegen::Codegen;
use syntax::parser;

mod syntax;
mod codegen;

fn main() -> Result<()> {
    let input = "{name, uname: u.username.lower()} for u in users where u.age > 13";

    println!("In           : {}", input);

    let cmd = parser::parse(input)?;

    println!("AST          : {:?}", cmd);

    let strict_output = Codegen::new(true).generate(&cmd)?;
    let loose_output = Codegen::new(false).generate(&cmd)?;

    println!("SQL (strict) : {}", strict_output);
    println!("SQL (loose)  : {}", loose_output);

    Ok(())
}
