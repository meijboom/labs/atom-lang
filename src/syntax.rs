#[derive(Debug)]
pub enum ComparisonOp {
    Lt,
    Lte,
    Gt,
    Gte,
    Eq,
    Neq,
}

#[derive(Debug)]
pub enum ArithmeticOp {
    Mul,
    Div,
    Add,
    Sub,
    BitAnd,
    BitOr,
}

#[derive(Debug)]
pub struct ComparisonExpr {
    pub left: Expr,
    pub right: Expr,
    pub op: ComparisonOp,
}

#[derive(Debug)]
pub struct ArithmeticExpr {
    pub left: Expr,
    pub right: Expr,
    pub op: ArithmeticOp,
}

#[derive(Debug)]
pub struct PropertyExpr {
    pub object: Expr,
    pub property: Expr,
}

#[derive(Debug)]
pub struct CallExpr {
    pub name: String,
    pub args: Vec<Expr>,
}

#[derive(Debug)]
pub enum Expr {
    Int(i64),
    Ident(String),
    Call(CallExpr),
    Not(Box<Expr>),
    Arithmetic(Box<ArithmeticExpr>),
    Comparison(Box<ComparisonExpr>),
    Property(Box<PropertyExpr>),
}

#[derive(Debug)]
pub struct Field {
    pub name: String,
    pub expr: Expr,
}

#[derive(Debug)]
pub struct Selector {
    pub name: Option<String>,
    pub table_name: String,
    pub filters: Option<Expr>,
}

#[derive(Debug)]
pub struct Select {
    pub fields: Vec<Field>,
    pub selector: Selector,
}

#[derive(Debug)]
pub enum Cmd {
    Select(Select),
}

// {studentID, five: 3+2, currentDate: now()} in student

peg::parser! {
    pub grammar parser() for str {
        rule whitespace()
            = [' ' | '\n']

        rule _()
            = whitespace()*

        rule __()
            = whitespace()+

        rule ident() -> String
            = name:$(['a'..='z' | 'A'..='Z']['a'..='z' | 'A'..='Z' | '0'..='9' | '_']*) { name.to_string() }

        rule ident_expr() -> Expr
            = name:ident() { Expr::Ident(name.to_string()) }

        rule call_expr() -> Expr
            = name:ident() "(" args:expr() ** (_ ",") ")" { Expr::Call(CallExpr { name, args }) }

        rule number_expr() -> Expr
            = num:$("-"? ['0'..='9']+) { Expr::Int(num.parse().unwrap()) }

        rule prefix() -> Expr
            = call_expr() / ident_expr() / number_expr()

        rule expr() -> Expr = precedence!{
            //left:(@) _ "||" _ right:@ { Node::new(left.pos, Expr::Logical(Logical { left, op: LogicalOp::Or, right }.into())) }
            //--
            //left:(@) _ "&&" _ right:@ { Node::new(left.pos, Expr::Logical(Logical { left, op: LogicalOp::And, right }.into())) }
            //--
            left:(@) _ "|" _ right:@ { Expr::Arithmetic(ArithmeticExpr { left, op: ArithmeticOp::BitOr, right }.into()) }
            left:(@) _ "&" _ right:@ { Expr::Arithmetic(ArithmeticExpr { left, op: ArithmeticOp::BitAnd, right }.into()) }
            --
            left:(@) _ "==" _ right:@ { Expr::Comparison(ComparisonExpr { left, op: ComparisonOp::Eq, right }.into()) }
            left:(@) _ "!=" _ right:@ { Expr::Comparison(ComparisonExpr { left, op: ComparisonOp::Neq, right }.into()) }
            --
            left:(@) _ ">" _ right:@ { Expr::Comparison(ComparisonExpr { left, op: ComparisonOp::Gt, right }.into()) }
            left:(@) _ ">=" _ right:@ { Expr::Comparison(ComparisonExpr { left, op: ComparisonOp::Gte, right }.into()) }
            --
            left:(@) _ "<" _ right:@ { Expr::Comparison(ComparisonExpr { left, op: ComparisonOp::Lt, right }.into()) }
            left:(@) _ "<=" _ right:@ { Expr::Comparison(ComparisonExpr { left, op: ComparisonOp::Lte, right }.into()) }
            --
            left:(@) _ "+" _ right:@ { Expr::Arithmetic(ArithmeticExpr { left, op: ArithmeticOp::Add, right }.into()) }
            left:(@) _ "-" _ right:@ { Expr::Arithmetic(ArithmeticExpr { left, op: ArithmeticOp::Sub, right }.into()) }
            --
            left:(@) _ "*" _ right:@ { Expr::Arithmetic(ArithmeticExpr { left, op: ArithmeticOp::Mul, right }.into()) }
            left:(@) _ "/" _ right:@ { Expr::Arithmetic(ArithmeticExpr { left, op: ArithmeticOp::Div, right }.into()) }
            --
            "!" _ expr:@ { Expr::Not(expr.into()) }
            --
            object:(@) _ "." _ property:@ { Expr::Property(PropertyExpr { object, property }.into()) }
            --
            "(" _ expr:expr() _ ")" { expr }
            --
            prefix:prefix() { prefix }
        }

        rule field_key() -> String
            = ident:ident() _ ":" _ { ident }

        rule field() -> Field
            = _ name:field_key()? expr:expr() _ {?
            match expr {
                Expr::Ident(ident) if name.is_none() => Ok(Field { name: ident.clone(), expr: Expr::Ident(ident) }),
                _ if name.is_none() => Err("field without name"),
                _ => Ok(Field { name: name.unwrap(), expr }),
            }
        }

        rule filters() -> Expr
            = __ "where" __ expr:expr() { expr }

        rule selector_for() -> String
            = "for" __ ident:ident() __ { ident }

        rule selector() -> Selector
            = name:selector_for()? "in" __ table_name:ident() filters:filters()? { Selector { table_name, name, filters } }

        rule select() -> Select
            = _ "{" _ fields:field() ** (_ ",") _ "}" __ selector:selector() _ { Select { fields, selector } }

        pub rule parse() -> Cmd
            = select:select() { Cmd::Select(select) }
    }
}